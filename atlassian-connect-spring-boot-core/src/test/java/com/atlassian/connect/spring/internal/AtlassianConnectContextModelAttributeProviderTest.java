package com.atlassian.connect.spring.internal;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.internal.auth.AtlassianConnectSecurityContextHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AtlassianConnectContextModelAttributeProviderTest {

    private static final String HOSTNAME = "http://context-host.com";

    private static final String CONTEXT_PATH = "/product";

    @Mock
    private HttpServletRequest request;

    @Mock
    private AtlassianConnectProperties atlassianConnectProperties;

    @Mock
    private AtlassianConnectSecurityContextHelper securityContextHelper;

    @InjectMocks
    private AtlassianConnectContextModelAttributeProvider modelAttributeProvider;

    @Test
    public void shouldReturnCDNAllJs() {
        assertThat(modelAttributeProvider.getAllJsUrl(), is("https://connect-cdn.atl-paas.net/all.js"));
    }
}
