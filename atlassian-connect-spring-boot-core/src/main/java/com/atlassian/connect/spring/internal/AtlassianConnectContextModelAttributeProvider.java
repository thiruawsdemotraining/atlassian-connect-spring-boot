package com.atlassian.connect.spring.internal;

import com.atlassian.connect.spring.internal.auth.AtlassianConnectSecurityContextHelper;
import com.atlassian.connect.spring.internal.request.jwt.SelfAuthenticationTokenGenerator;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpServletRequest;

/**
 * A controller utility class that maps the standard Atlassian Connect iframe context parameters to Spring model
 * attributes.
 */
@ControllerAdvice
public class AtlassianConnectContextModelAttributeProvider {

    private static final String ALL_JS_URL = "https://connect-cdn.atl-paas.net/all.js";

    private static final String ALL_DEBUG_JS_URL = "https://connect-cdn.atl-paas.net/all-debug.js";

    private HttpServletRequest request;

    private AtlassianConnectProperties atlassianConnectProperties;

    private AtlassianConnectSecurityContextHelper securityContextHelper;

    private SelfAuthenticationTokenGenerator selfAuthenticationTokenGenerator;

    public AtlassianConnectContextModelAttributeProvider(HttpServletRequest request,
            AtlassianConnectProperties atlassianConnectProperties,
            AtlassianConnectSecurityContextHelper securityContextHelper,
            SelfAuthenticationTokenGenerator selfAuthenticationTokenGenerator) {
        this.request = request;
        this.atlassianConnectProperties = atlassianConnectProperties;
        this.securityContextHelper = securityContextHelper;
        this.selfAuthenticationTokenGenerator = selfAuthenticationTokenGenerator;
    }

    @ModelAttribute
    public void addAttributes(Model model) {
        addModelAttribute(model, "atlassianConnectLicense", getLicense());
        addModelAttribute(model, "atlassianConnectAllJsUrl", getAllJsUrl());
        addModelAttribute(model, "atlassianConnectToken", getSelfAuthenticationToken());
    }

    public String getLicense() {
        return request.getParameter("lic");
    }

    public String getAllJsUrl() {
        return atlassianConnectProperties.isDebugAllJs() ? ALL_DEBUG_JS_URL : ALL_JS_URL;
    }

    public String getSelfAuthenticationToken() {
        return securityContextHelper.getHostUserFromSecurityContext()
                .map(selfAuthenticationTokenGenerator::createSelfAuthenticationToken)
                .orElse(null);
    }

    private void addModelAttribute(Model model, String attributeName, String value) {
        String dashedAttributeName = String.join("-", attributeName.split("(?=\\p{Upper})")).toLowerCase();
        model.addAttribute(attributeName, value);
        model.addAttribute(dashedAttributeName, value);
    }
}
