package com.atlassian.connect.spring.internal.auth.jwt;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * An authentication exception thrown when processing a JSON Web Token that could not be successfully parsed.
 */
@ResponseStatus(code = HttpStatus.UNAUTHORIZED, reason = "Invalid symmetric JWT for install hook")
public class InvalidSymmetricJwtException extends AuthenticationException {
    public InvalidSymmetricJwtException(String message) {
        super(message);
    }
}
