package com.atlassian.connect.spring.internal.jwt;

import com.nimbusds.jose.Algorithm;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.crypto.MACVerifier;

public class SymmetricJwtReader extends AbstractJwtReader {

    public SymmetricJwtReader(MACVerifier verifier) {
        super(verifier);
    }

    @Override
    protected Algorithm getSupportedAlgorithm() {
        return JWSAlgorithm.HS256;
    }
}
