package com.atlassian.connect.spring.it.util;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.internal.jwt.CanonicalHttpRequest;
import com.atlassian.connect.spring.internal.request.AtlassianConnectHttpRequestInterceptor;
import com.atlassian.connect.spring.internal.request.AtlassianHostUriResolver;
import com.atlassian.connect.spring.internal.request.jwt.JwtBuilder;
import com.atlassian.connect.spring.internal.request.jwt.JwtQueryHashGenerator;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpRequest;

import java.net.URI;
import java.security.interfaces.RSAPrivateKey;
import java.util.Map;
import java.util.Optional;

public class AsymmetricJwtSigningClientHttpRequestInterceptor extends AtlassianConnectHttpRequestInterceptor {
    private final String appBaseUrl;
    private final String clientKey;
    private final RSAPrivateKey privateKey;
    private final String keyId;
    private final Optional<String> optionalSubject;
    private final JwtQueryHashGenerator queryHashGenerator = new JwtQueryHashGenerator();
    private final Optional<Map<String, Object>> optionalContext;


    public AsymmetricJwtSigningClientHttpRequestInterceptor(String clientKey, String appBaseUrl, RSAPrivateKey privateKey, String keyId, Optional<String> optionalSubject, Optional<Map<String, Object>> optionalContext) {
        super("1");
        this.appBaseUrl = appBaseUrl;
        this.clientKey = clientKey;
        this.privateKey = privateKey;
        this.keyId = keyId;
        this.optionalSubject = optionalSubject;
        this.optionalContext = optionalContext;
    }


    @Override
    protected Optional<AtlassianHost> getHostForRequest(HttpRequest request) {
        AtlassianHost host = new AtlassianHost();
        host.setBaseUrl(AtlassianHosts.BASE_URL);
        return Optional.of(host);
    }

    @Override
    protected HttpRequest rewrapRequest(HttpRequest request, AtlassianHost host) {
        request.getHeaders().set(HttpHeaders.AUTHORIZATION, String.format("JWT %s", createJwt(request.getMethod(), request.getURI(), host.getBaseUrl())));
        return request;
    }

    public String createJwt(HttpMethod method, URI uri, String baseUrl) {
        CanonicalHttpRequest canonicalHttpRequest = queryHashGenerator.createCanonicalHttpRequest(
                method, uri, AtlassianHostUriResolver.getBaseUrl(uri));
        String queryHash = queryHashGenerator.computeCanonicalRequestHash(canonicalHttpRequest);
        AsymmetricJWTBuilder jwtBuilder = new AsymmetricJWTBuilder()
                .issuer(clientKey)
                .queryHash(queryHash)
                .audience(appBaseUrl)
                .asymmetricSignature(privateKey, keyId);
        optionalSubject.ifPresent(jwtBuilder::subject);
        optionalContext.ifPresent(context -> jwtBuilder.claim("context", context));
        return jwtBuilder.build();
    }
}
